import tool_library.timer as timer
import tool_library.version as version
import tool_library.about as about
import tool_library.make_qr as qr
import tool_library.task as task
import tool_library.help as help
import tool_library.file_download as down

import time
import sys


# 主界面
def mainMenu():
    print("工具箱1.0 by Gitee/JuRuoqwq")
    print("功能：")
    # 这里如果用\t会很难看
    print(
        """1.计时器(--timer)  2.二维码生成(--qr)    3.系统版本查看(--sysver)    4.进程管理(--task)   
        5.多线程下载(--down)    6.关于(--about)    7.帮助(--help)  8.退出工具箱(--quit)""")
    res = input("Option $ ")
    return res

# 解析用户输入
def parseOption():
    try:
        while True:
            # 因为是while True，所以可能导致上次的输出刚刚显示，用户来不及看，下一次的输出就出来了
            time.sleep(1.5)
            oper = mainMenu()
            if oper == "--timer":
                timer.timer()

            elif oper == "--qr":
                qr.makeQRCode()

            elif oper == "--sysver":
                version.showAll()

            elif oper == "--task":
                task.runOption()

            elif oper == "--down":
                down.main()

            elif oper == "--about":
                about.printInfo()

            elif oper == "--help":
                help.helpMenu()

            elif oper == "--quit":
                sys.exit(0)

            else:
                print("Invalid option")
    except KeyboardInterrupt:
        sys.exit(0)


if __name__ == "__main__":
    parseOption()