# The PyToolBox by JuRuoqwq
### What is this?
> PyToolBox is a toolbox based on Python 3
> Based on CLI production, including four functions: timer, QR code generation, system version viewing, Internet file download and process management
### How to use it
This project includes a complete dependency installation and startup script
> Enter the following script in the terminal

```
./main.exe
```

### About PR
If you want to submit a Pull Request, please make sure to meet the following requirements
1. Programming using Python 3
2. Familiar with BAT&Shell and proficient in Python
3. Use SVN for version management