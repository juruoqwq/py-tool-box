# 导入库
import time
import winsound as ws

# 获取用户输入
def getInputTime():
    #初始化变量
    hour = 0
    minute = 0
    second = 0

    print("请输入要计时的时间(如果不到请填0)")
    hour = int(input("小时数 $ "))
    minute = int(input("分钟数 $ "))
    second = int(input("秒数 $ "))

    result = hour * 60 * 60 + minute * 60 + second
    return result

# 计时函数
def timer():
    second = getInputTime()
    if second == 0: return
    while True:
        time.sleep(1.0) # Python运行速度较慢，所以少了0.1秒
        second -= 1
        print("还剩{}秒".format(second))

        if second == 0:
            print("时间到，计时结束！\n")
            for i in range(0,15):
                ws.Beep(900, 500)
            return 
# 测试
if __name__ == "__main__":
    timer()