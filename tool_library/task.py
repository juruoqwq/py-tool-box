# 导入库
import os

# 主界面
def taskMenu():
    print("")
    print("进程助手 by JuRuoqwq")
    print("list:进程列表   kill:杀死进程")

# 获取指令
def getOption():
    taskMenu()
    option = input("Option $ ")
    return option

#执行指令
def runOption():
    option = getOption()
    # Windows系统
    if option == "list":
        os.system("tasklist")
    elif option == "kill":
        task = input("Taskname $ ")
        os.system("taskkill /f /t /im " + task)
    else:
        print("Invalid Option")
    return

#测试
if __name__ == "__main__":
    runOption()