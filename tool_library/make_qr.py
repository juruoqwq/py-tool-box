
# 导入库
import os
import pyqrcode


# 获取要生成二维码的数据
def getUserInput():
    data = input("请输入想生成二维码的数据(仅限英语，数字) $ ")
    return data

# 生成QRCode
def makeQRCode():
    url = pyqrcode.create(getUserInput())
    # 生成 png 文件之前需先安装pypng(默认不安装，所以需要run.bat)
    url.png('./QRCode.png', scale=5, module_color="#7D007D", background=[0xff, 0xff, 0xcc])
    print("已保存二维码！\n")    
    os.system(".\\QRCode.png")

# 测试
if __name__ == "__main__":
    makeQRCode()