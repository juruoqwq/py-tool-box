def helpMenu():
    helpString = """Option $ [option]
                        --timer  Start the timer tool
                        --qr     Start the MakeQR tool
                        --sysver Show your system's info
                        --task   Start the task manager tool
                        --down   download file tool
                        --about  Show the toolbox's info
                        --help   Show this message
                        --quit   Quit the tool box\n"""
    print(helpString)