# 导入库
import tool_library.data as data

# 唯一一个函数
def printInfo():
    data.showInfo("软件名",data.softwareName)
    data.showInfo("软件版本",data.softwareVersion)
    data.showInfo("作者",data.softwareAuthor)
    data.showInfo("Gitee仓库",data.giteeLink)
    print("\n")
    return

# 测试
if __name__ == "__main__":
    printInfo()