# 导入库
import platform
import tool_library.data as data

# 输出系统信息
def showAll():
    data.showInfo("操作系统及版本信息",platform.platform())
    data.showInfo("获取系统版本号",platform.version())
    data.showInfo("获取系统名称", platform.system())
    data.showInfo("系统位数", platform.architecture())
    data.showInfo("CPU架构", platform.machine())
    data.showInfo("计算机名称", platform.node())
    data.showInfo("CPU类型", platform.processor())
    print("\n")
    return

# 测试
if __name__ == "__main__":
    showAll()