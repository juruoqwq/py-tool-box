# The PyToolBox by JuRuoqwq
### 这是什么？
> PyToolBox是一个基于Python3制作的工具箱
> 基于CLI制作，包含了计时器，二维码生成，系统版本查看，多线程下载文件和进程管理五个功能
### 如何使用它
本项目包含了完整的依赖安装&启动脚本
> 在终端中输入以下脚本

```
./main.exe
```


### 关于PR
如果您想提交一个Pull Request，请务必满足以下要求
1. 使用Python3编程
2. 熟悉BAT&Shell，精通Python
3. 使用SVN进行版本管理